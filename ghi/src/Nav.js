import { Button, Container, Nav, Navbar } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { FaUser } from "react-icons/fa";
import React, { useState, useEffect } from "react";
import campLogo from '../src/img/campLogo.jpg';

function CampNav() {
    const { logout, token } = useToken();
    const { fetchWithCookie } = useToken();
    // const [accountData, setAccountData] = useState("");
    const [account_id, setAccountId] = useState(null);

    const getAccountData = async () => {
        const accountData = await fetchWithCookie(`${process.env.REACT_APP_API_HOST}/token`);
        // setAccountData(accountData.account);
        setAccountId(accountData.account.id);
    };

    useEffect(() => {
        if (token) {
        getAccountData();
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [token]);

    const profileURL = `/profile/${account_id}`;

    return (
      <Navbar bg="white" data-bs-theme="dark" sticky="top">
        <Container fluid>
          <NavLink to="/">
            <img
                src={campLogo}
                alt="Your Alt Text"
                width="75px"
                height="45px"
                className="d-inline-block align-top"
            />{' '}
            </NavLink>
          <NavLink to="/" className="Title">
            Home
          </NavLink>

          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="justify-content-end flex-grow-1 pe-3">
              {!token && (
                <NavLink to="/login">
                  <Button
                    style={{ backgroundColor: "#464F2E", color: "white", marginRight: "15px" }}
                  >
                    LogIn
                  </Button>
                </NavLink>
              )}

              {token && (
                <NavLink to="/">
                  <Button
                    style={{ backgroundColor: "#464F2E", color: "white" }}
                    variant="success"
                    onClick={logout}
                  >
                    LogOut
                  </Button>
                </NavLink>
              )}
              {!token && (
                <NavLink to="/signup">
                  <Button
                    style={{ backgroundColor: "#464F2E", color: "white" }}
                  >
                    SignUp
                  </Button>
                </NavLink>
              )}
            </Nav>

            <NavLink to={profileURL} className="profile button">
              {token && (
                <FaUser
                  style={{ color: "#464F2E", fontSize: "30px", margin: "15px" }}
                />
              )}
            </NavLink>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    );
}

export default CampNav;
