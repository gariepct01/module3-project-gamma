from main import app
from fastapi.testclient import TestClient

client = TestClient(app)


def test_get_facilities():
    response = client.get("/api/facilities", params={"state_code": "NC"})
    assert response.status_code == 200


def test_bad_state_code():
    response = client.get("/api/facilities", params={"state_code": "12"})
    data = response.json()
    assert data is None


def test_get_facility_details():
    response = client.get(
        "/api/facility_details",
        params={
            "park_code": "grsm",
            "facility_id": "1241C56B-7003-4FDF-A449-29DA8BCB0A41",
        },
    )
    assert response.status_code == 200
